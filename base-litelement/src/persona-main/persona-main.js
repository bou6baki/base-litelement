import { LitElement, html, css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

 
class PersonaMain extends LitElement {
    // los estilos se heredan hacia dentro. Esto interrumpe los estilos de fuera y los pone nuevos hacia dentro
    // Hay algunos que no se heredan
/*     static get styles () {
        return css `
            :host {
                all: initial;
            }
        `;
    } */

    static get properties () {
        return {
            people: {type: Array },
            showPersonForm : {type: Boolean}
        };
    }

    constructor () {
        super();
        this.people = [
            {
                name: "Mikkel Nielsen",
                yearsInCompany: 12,
                photo: {
                    "src" : "https://randomuser.me/api/portraits/men/66.jpg",
                    "alt" : "Photo Id Picture"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor "
            },
            {
                name: "Jonas Kahnwald",
                yearsInCompany: 1,
                photo: {
                    "src" : "https://randomuser.me/api/portraits/men/32.jpg",
                    "alt" : "Photo Id Picture"
                },
                profile: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia "
            },
            {
                name: "Claudia Tiedemann",
                yearsInCompany: 18,
                photo: {
                    "src" : "https://randomuser.me/api/portraits/women/75.jpg",
                    "alt" : "Photo Id Picture"
                },
                profile: "ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo"
            },
            {
                name: "Ulrich Nielsen",
                yearsInCompany: 5,
                photo: {
                    "src" : "https://randomuser.me/api/portraits/men/43.jpg",
                    "alt" : "Photo Id Picture"
                },
                profile: "Ut enim ad minima veniam, quis nostrum exercitationem  "
            },
            {
                name: "Hanna Kahnwald",
                yearsInCompany: 7,
                photo: {
                    "src" : "https://randomuser.me/api/portraits/women/66.jpg",
                    "alt" : "Photo Id Picture"
                },
                profile: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium "
            }
        ];
        // usamos esta propiedad para guardar el estado si monstramos o no el formulario de nueva persona
        this.showPersonForm = false;
    }

    updated(changedProperties) {
        console.log("updated en persona-main");

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en pesona-main");
            //comprobamos si queremos mostrsr el formulario o el listado
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(
                new CustomEvent("updated-people",{detail: {people: this.people}}) 
            )
        }
    }

    showPersonList() {
        console.log("showPersonList");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    render() {

        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html `<persona-ficha-listado
                                            @delete-person="${this.deletePerson}"
                                            @info-person="${this.infoPerson}"
                                            fname="${person.name}" 
                                            fyearsInCompany="${person.yearsInCompany}"
                                            profile="${person.profile}"
                                            .photo="${person.photo}" >
                                        </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                    @persona-form-store="${this.personFormStore}"
                    @persona-form-close="${this.personFormClose}" 
                    id="personForm" 
                    class="d-none border rounded border-primary">
                </persona-form>
            </div>
        `;
    }
    infoPerson (e) {
        console.log("infoPerson");
        console.log("Se ha pedido nombre de la persona: " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        // console.log(chosenPerson);
        //aca pasamos los datos de la pesona seleccionada al formulario, al objeto dentro del formulario
        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    } 

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Payload: " + JSON.stringify(e.detail));

        this.people = this.people.filter(
            person => person.name != e.detail.name 
        );
    };

    personFormStore(e) {
        console.log("personFormStore");
        
        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar: " + e.detail.person.name);
            // buscamos el indice de donde esta el elemento que estamos modificando. devuelve unindice donde esta

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                            ? person = e.detail.person : person 
            );

            // let indexOfPerson = 
            //     this.people.findIndex(
            //         person => person.name === e.detail.person.name
            //     ); 
            // if (indexOfPerson >= 0) { //ha encontrado uno
            //     console.log("Persona encontrada");
            //     this.people[indexOfPerson] = e.detail.person;
            // }
        } else {
            console.log("Se va a almacenar una persona: " + JSON.stringify(e.detail.person));
            // this.people.push(e.detail.person);
            this.people = [...this.people,e.detail.person]; //spreasheet en js
        }

        
        
        console.log("Proceso terminado de alta/update ");
        this.showPersonForm = false;  
    }
    personFormClose(e){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de alta");

        this.showPersonForm = false;

    }

}

customElements.define('persona-main', PersonaMain)