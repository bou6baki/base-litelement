import { LitElement, html} from 'lit-element';

 
class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }s
    constructor() {
        super();
        this.name = "El calvo de Telecinco";
        this.yearsInCompany = 1;
        this.photo = {
            src: "https://randomuser.me/api/portraits/men/75.jpg",
            alt: "Foto persona humana"
        };

        this.updatePersonInfo();
        
    }

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) => {
            console.log("La propiedad: " + propName + " tenia el valor: " + oldValue)
        });

        if (changedProperties.has("name")) {
            console.log("Propiedad name cambia de valor. El valor era: " + changedProperties.get("name") + " nuevo es " + this.name);
        };

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad YEARS cambia de valor. El valor era: " + changedProperties.get("name") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        };
    }

    render() {
        return html`
            <div>
                <label for="fname">Nombre Completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @change="${this.updateName}"></input>
                </br>
                <label for="yearsInCompany">Años en la empresa</label>
                <input type="number" name="yearsInCompany" value=${this.yearsInCompany} @change="${this.updateYearsInCompany}"></input>
                </br>
                <label for="pinfo">Info de la persona</label>
                <input type="text" disabled name="pinfo" value="${this.personInfo}" ></input>
                </br>
                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>

            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo(e){
        console.log("updatePersonInfo");

        
        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }

}