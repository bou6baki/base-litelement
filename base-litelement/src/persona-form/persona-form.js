import { LitElement, html} from 'lit-element';

 

class personaForm extends LitElement {
    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor() {
        super();

        this.resetFormData();
    }
// en el campo de entrada input, el .value (con punto) es la propiedad de la que toma valor el atributo (sin punto)
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}"  id="personFormName" class="form-control" placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" type="text" class="form-control" placeholder="Años en la empresa"/>
                    </div>
                    <button @click="${this.goBack}" class="btn btn-secondary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>

        `;
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor: " + e.target.value);

        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor: " + e.target.value);

        this.person.profile = e.target.value;

    }

    updateYearsInCompany (e) {
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor: " + e.target.value);

        this.person.yearsInCompany = e.target.value;
    }

    goBack (e) {
        console.log("goBack");
        e.preventDefault();

        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        this.resetFormData();
    }

    storePerson (e) {
        console.log("storePerson");
        let imgnumber = Math.floor(Math.random() * 99);

        e.preventDefault();
        if (this.editingPerson === false) {
            this.person.photo = {
                "src": `https://randomuser.me/api/portraits/men/${imgnumber}.jpg`,
                "alt": "Person Image" 
            }
        }   

        console.log("objeto a enviar name: " + this.person.name);
        console.log("objeto a enviar profile: " + this.person.profile);
        console.log("objeto a enviar years: " + this.person.yearsInCompany);
        console.log("objeto a enviar img: " + this.person.photo);
        //se puede pasar el objeto completo, pero si lo hacemos asi controlamos exactamente que pasamos
        this.dispatchEvent(
            new CustomEvent(
                "persona-form-store",
                {
                    "detail": {
                        person: { 
                            "name": this.person.name,
                            "yearsInCompany": this.person.yearsInCompany,
                            "profile": this.person.profile,
                            "photo": this.person.photo 
                        },
                        editingPerson: this.editingPerson
                    }
                }
            )
        );
        this.resetFormData();
    }

    resetFormData() {
        console.log("");

        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.editingPerson = false;
    }

}

customElements.define('persona-form', personaForm)