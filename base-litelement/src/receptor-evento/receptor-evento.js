import { LitElement, html} from 'lit-element';

 

class ReceptorEvento extends LitElement {
    static get properties() {
        return {
            course: {type: String},
            year: {type: String}
        }
    }
    render() {
        return html`

            <h3>ReceptorEvento</h3>
            <h4>Este curso es de ${this.course}</h4>
            <br>
            <h4>y estamos en el año ${this.year}</h4>

        `;

    }

}

 

customElements.define('receptor-evento', ReceptorEvento)