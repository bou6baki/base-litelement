import { LitElement, html} from 'lit-element';

 

class TestApi extends LitElement {

    static get properties() {
        return {
            movies: {type: Array}
        }
    }

    constructor () {
        super();

        this.movies = [];
        this.getMovieData();

    }
    render() {
        return html`
            ${this.movies.map(
                movie =>
                html `<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}.</div>`
            )}
        `;

    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de las peliculas..");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
             if (xhr.status === 200) {
                 console.log("Peticion completada correctamente");

                 let APIResponse = JSON.parse(xhr.responseText);
                 this.movies = APIResponse.results;
             }           
        }

        xhr.open("GET","https://swapi.dev/api/films/");
        xhr.send();

    }

}

 

customElements.define('test-api', TestApi)