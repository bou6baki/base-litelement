import { LitElement, html} from 'lit-element';

 

class EmisorEvento extends LitElement {

    render() {

        return html`

            <h3>Emisor EmisorEvento</h3>
            <button @click="${this.sendEvent}">No tocar</button>

        `;

    }
    //La siguiente funcion es un custom, asi que el nombre es cualquiera
    sendEvent(e) {
        console.log("sendEvent");
        console.log("pulsado el boton");

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail": {
                        "course": "techu",
                        "year": 2000
                    }
                }
            )
        );
    }
}

 

customElements.define('emisor-evento', EmisorEvento)