import { LitElement, html} from 'lit-element';

 
class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname: {type:String},
            fyearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        };
    }s
    constructor() {
        super();
        
    }


    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="250" width="1" class="card-img-top" ></img>    
                <div class="card-body">
                    <h5 class="card-title ">${this.fname}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.fyearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer" >
                    <button class="btn btn-danger col-5" @click="${this.deletePerson}"><strong>Delete</strong></button>
                    <button class="btn btn-info col-5 offset-1" @click="${this.moreInfo}"><strong>Info</strong></button>
                </div>
            </div>
        `;
    }

    moreInfo (e){
        console.log("moreInfo");
        console.log("Se ha pedido info de: " + this.fname);
        
        this.dispatchEvent(
            new CustomEvent(
                "info-person",
                {
                    detail: {
                        name: this.fname
                    }
                }
            )
        );
    } 

    deletePerson(e) {
        console.log("deletePerson");
        console.log("Vamos a borrar a: " + this.fname);

        this.dispatchEvent(
            new CustomEvent(
                "delete-person",
                {
                    detail: {
                        name: this.fname
                    }
                }
            )
        );
    }    

}


customElements.define('persona-ficha-listado', PersonaFichaListado)